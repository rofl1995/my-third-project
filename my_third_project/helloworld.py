"""My first Module"""

def print_me(text):
    """Ausgabe eines Textes."""
    print(text)

if __name__ == "__main__":
    print_me("Hello World")
